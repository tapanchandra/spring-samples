package com.dryrun.java.spring.samples.scopes;

import com.dryrun.java.spring.samples.HelloWorld;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class SingletonSample {

    public void run() {

        ApplicationContext context = new ClassPathXmlApplicationContext("SingletonBean.xml");
        HelloWorld objA = (HelloWorld) context.getBean("helloWorld");

        objA.setMessage("I'm object A");
        objA.getMessage();

        HelloWorld objB = (HelloWorld) context.getBean("helloWorld");
        objB.getMessage();
    }
}
