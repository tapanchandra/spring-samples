package com.dryrun.java.spring.samples.beanpostprocessor;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;

public class MyProcessor implements BeanPostProcessor {
    public Object postProcessBeforeInitialization(Object bean, String beanName)
            throws BeansException {

        //Original value of message
        ((HelloWorld)bean).getMessage();

        System.out.println("BeforeInitialization : " + beanName);
        ((HelloWorld)bean).setMessage("msg replaced at before initialization");

        //Message Now is
        ((HelloWorld)bean).getMessage();
        return bean;  // you can return any other object as well
    }
    public Object postProcessAfterInitialization(Object bean, String beanName)
            throws BeansException {

        System.out.println("AfterInitialization : " + beanName);
        ((HelloWorld)bean).setMessage("msg replaced after initialization");

        //Message Now is
        ((HelloWorld)bean).getMessage();
        return bean;  // you can return any other object as well
    }
}
