package com.dryrun.java.spring.samples.innerbeans;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class InnerBeansSample {

    public void run() {
        ApplicationContext context = new ClassPathXmlApplicationContext("innerbeans.xml");
        TextEditor te = (TextEditor) context.getBean("textEditor");
        te.spellCheck();
    }
}
