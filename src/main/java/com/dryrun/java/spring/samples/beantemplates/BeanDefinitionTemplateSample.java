package com.dryrun.java.spring.samples.beantemplates;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class BeanDefinitionTemplateSample {

    public void run() {

        ApplicationContext context = new ClassPathXmlApplicationContext("beandefinitiontemplatesample.xml");

        Hello2 objA = (Hello2) context.getBean("helloagain");
        objA.getMessage1();
        objA.getMessage2();


    }
}
