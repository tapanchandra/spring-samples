package com.dryrun.java.spring.samples.beanpostprocessor;

public class HelloWorld {
    private String message;

    public void setMessage(String message){
        this.message  = message;
    }
    public void getMessage(){
        System.out.println("Your Message : " + message);
    }
    public void init(){
        System.out.println("Bean is going through init.");
        System.out.println("message value during init is " + this.message);
    }
    public void destroy(){
        System.out.println("Bean will destroy now...");
    }
}