package com.dryrun.java.spring.samples.beanpostprocessor;


import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class BeanPostProcessorSample {
    public void run() {
        AbstractApplicationContext context = new ClassPathXmlApplicationContext("BeanPostProcessor-Beans.xml");

        HelloWorld obj = (HelloWorld) context.getBean("helloWorld");
        obj.getMessage();
        context.registerShutdownHook();
    }

}
