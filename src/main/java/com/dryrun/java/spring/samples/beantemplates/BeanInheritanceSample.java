package com.dryrun.java.spring.samples.beantemplates;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class BeanInheritanceSample {

    public void run() {
        ApplicationContext context = new ClassPathXmlApplicationContext("bean-inheritance.xml");

        HelloWorld objA = (HelloWorld) context.getBean("helloWorld");
        objA.getMessage1();
        objA.getMessage2();

        Hello2 objB = (Hello2) context.getBean("hello2");
        objB.getMessage1();
        objB.getMessage2();
        objB.getMessage3();
    }
}
