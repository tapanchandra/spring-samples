package com.dryrun.java.spring.samples.lifecycle;

import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class LifecycleSample {

    public void run() {

        AbstractApplicationContext context = new ClassPathXmlApplicationContext("LifecycleSampleBeans.xml");

        HelloWorld obj = (HelloWorld) context.getBean("helloWorld");
        obj.getMessage();
        context.registerShutdownHook();

    }
}
