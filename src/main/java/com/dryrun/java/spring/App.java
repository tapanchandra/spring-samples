package com.dryrun.java.spring;

import com.dryrun.java.spring.samples.beanpostprocessor.BeanPostProcessorSample;
import com.dryrun.java.spring.samples.beantemplates.BeanDefinitionTemplateSample;
import com.dryrun.java.spring.samples.beantemplates.BeanInheritanceSample;
import com.dryrun.java.spring.samples.helloworld.HelloWorldSample;
import com.dryrun.java.spring.samples.innerbeans.InnerBeansSample;
import com.dryrun.java.spring.samples.lifecycle.LifecycleSample;
import com.dryrun.java.spring.samples.scopes.PrototypeSample;
import com.dryrun.java.spring.samples.scopes.SingletonSample;

public class App {

    public static void main(String[] args) {

        new InnerBeansSample().run();

    }

    public static void runAll() {

        new HelloWorldSample().run();

        new SingletonSample().run();

        new PrototypeSample().run();

        new LifecycleSample().run();

        new BeanPostProcessorSample().run();

        new BeanInheritanceSample().run();

        new BeanDefinitionTemplateSample().run();

    }
}
